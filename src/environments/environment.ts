// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAhkNHXLu6EkCmSU9CHPgNv66yV3kDT3C0",
    authDomain: "test2021-3104f.firebaseapp.com",
    projectId: "test2021-3104f",
    storageBucket: "test2021-3104f.appspot.com",
    messagingSenderId: "451118305490",
    appId: "1:451118305490:web:0f6fb4a0c8ff85b5bb1d0b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
