import { PredictionService } from './../prediction.service';
import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { validateEventsArray } from '@angular/fire/firestore';



@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  students$;
  userId: string;
  students: Student[];
  name: string;
  mathAverage: number;
  psych: number;
  paid: boolean;
  predict$;
  


  //disableSelect = new FormControl(false);

  cities: Object[] = [{ id: 1, name: 'paid', value: true }, { id: 2, name: 'NOT paid', value: false }]
  city: string;


  toppings = new FormControl();

  toppingList: string[] = ['paid', 'NOT paid'];

  constructor(private studentsService: StudentsService,
    public authService: AuthService,
    private router: Router,
    private predictionService:PredictionService) { }


    ngOnInit(): void {

      this.authService.getUser().subscribe(
        user => {
          this.userId = user.uid;
          console.log(this.userId);
          this.students$ = this.studentsService.getStudents(this.userId);
  
        }
      )
  
    }
    
  onSubmit() {
    this.router.navigate(['/students', this.city]);
  }

  onSelect(selectedPaymentState: any): void {
    this.paid = selectedPaymentState.value.value;
  }




}
