export interface Student {
    id:string,
    name:string,
    mathAverage:number,
    psych:number,
    paid:boolean,
    pred:number
}
