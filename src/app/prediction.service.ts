import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {

  url = 'https://xhapwevjyc.execute-api.us-east-1.amazonaws.com/beta'; 
  
  predict(mathAverage:number, psych:number,  paid:boolean):Observable<any>{
    let json = {
      "data": 
        {
          "mathAverage": mathAverage,
          "psych": psych,
          "paid":paid
        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

  constructor(private http: HttpClient) { }
}
